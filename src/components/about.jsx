import React from "react";
import myImage from "../img/myImage.png";

class About extends React.Component {
  constructor() {
    super();
    this.state = {
      skills: [
        { id: "C/C++_skill", content: "C/C++"},
        {
          id: "JavaScript_skill",
          content: "JavaScript"
          
        },
        { id: "KiCad_skill", content: "KiCad"},
        {
          id: "MS Office_skill",
          content: "MS Office"
          
        },
        {
          id: "Adobe Photoshop_skill",
          content: "Adobe Photoshop"
          
        },
        { 
          id: "Languages_skill", 
          content: "Armenian/Eglish/Russian/German"
        },
       
      ],
      about_me: [
        {
          id: "first-p-about",
          content:
            "I am Tigran Hakobyan, an aspiring engineer from Armenia. Since I was 3, I was a huge fan of legos and used to play with them a lot. I was creating imaginary robots and honoring them with magic functionalities. My parents noticed my love for robots and when I became 12 they took me to TUMO Centre for Creative Technologies, where I had the opportunity to study Robotics and Programming. Guess what?! I loved it even more and decided to endeep my knowledge in these fie;ds. When I began attending “Ayb” high school, I became part of Fab Lab - internationally qualified robotics lab. We started to create robots from scratch, making every detail by ourselves, and program the robots by the programming language C. Currently, we are working on a robot to participate in the National Robotics Challenge in the US."   
            
        },          
      ]
    };
  }

  render() {
    return (
      <section id="about" className="about-mf sect-pt4 route">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="box-shadow-full">
                <div className="row">
                  <div className="col-md-6">
                  <div className="title-s">
                        <h5 className="title-left">My Skills</h5>
                      </div>
                    <div className="row">
                      <div
                        className="col-sm-6 col-md-5"
                        style={{ margin: "0 auto" }}
                      >
                        <div
                          className="about-img"
                          style={{ textAlign: "center" }}
                        >
                          <img
                            className="img-fluid rounded b-shadow-a"
                            alt=""
                          />
                        </div>
                      </div>
                    </div>
                    <div className="skill-mf">
                      {/* <p className="title-s">Skill</p> */}
                      <ul className="dots">
                      {this.state.skills.map(skill => {
                        return (
                          <React.Fragment key={skill.id}>
                            <li><p>{skill.content}</p></li>{""}
                            
                          </React.Fragment>
                        );
                      })}
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="about-me pt-4 pt-md-0">
                      <div className="title-box-2">
                        <h5 className="title-left">About Me</h5>
                      </div>
                      {this.state.about_me.map(content => {
                        return (
                          <p className="lead" key={content.id}>
                            {content.content}
                          </p>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default About;
